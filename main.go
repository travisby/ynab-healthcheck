package main

import (
	"fmt"
	"os"
	"log"
	"net/http"
	"net/url"
	"encoding/json"
	"io/ioutil"
	"context"

	ynab "gitlab.com/travisby/go-ynab"
)

func rootHandler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintf(w, "Welcome to the ynab-toolkit!  There's nothing here right now ;)")
}

func privacyHandler(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintf(w, `
	1. If you decide to use some features that are tagged [server side] we may store budget data for you in a heroku postgres database (which is on AWS).
	2. We will never share access of our database to anyone else.
	`)
}

func oauthYNABHandler(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
	if code == "" {
		w.WriteHeader(422)
		return
	}
	log.Printf("code: %s", code)

	u, err := url.Parse(fmt.Sprintf("%s/oauth/token", os.Getenv("YNAB_API_URL")))
	if err != nil {
		log.Print(err)
		w.WriteHeader(500)
		return
	}

	q := u.Query()
	q.Set("client_id", os.Getenv("CLIENT_ID"))
	q.Set("client_secret", os.Getenv("CLIENT_SECRET"))
	q.Set("redirect_uri", os.Getenv("REDIRECT_URI"))
	q.Set("grant_type", "authorization_code")
	q.Set("code", code)
	q.Set("state", "hunter2")
	u.RawQuery = q.Encode()

	log.Printf("Sending to %s", u)

	res, err := http.DefaultClient.Post(u.String(), "application/json", nil)
	if err != nil {
		log.Print(err)
		w.WriteHeader(500)
		return
	}

	body := struct{
		AccessToken string `json:"access_token"`
		TokenType string `json:"token_type"`
		ExpiresIn int `json:"expires_in"`
		RefreshToken string `json:"refresh_token"`
	}{}

	bs, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Print(err)
		w.WriteHeader(500)
		return
	}
	log.Printf("got: %s\n", bs)

	// if err := json.NewDecoder(res.Body).Decode(&body); err != nil {
	if err := json.Unmarshal(bs, &body); err != nil {
		log.Print(err)
		w.WriteHeader(500)
		return
	}

	log.Printf("access token: %+v", body)

	cl := ynab.NewAPIClient(ynab.NewConfiguration())
	ctx := context.WithValue(context.Background(), ynab.ContextAccessToken, body.AccessToken)
	user, _, err := cl.UserApi.GetUser(ctx)
	if err != nil {
		log.Print(err)
		w.WriteHeader(500)
		return
	} else if user.Data == nil || user.Data.User == nil {
		log.Printf("user or data is nil")
		w.WriteHeader(500)
		return
	}
	log.Printf("%+v", *user.Data.User)

}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/privacy-policy", privacyHandler)
	http.HandleFunc("/oauth/ynab", oauthYNABHandler)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
